
#include <RFM12B.h>
#include <CommandHandler.h>
#include <EventQueue.h>
#include <RFM12bNode.h>


void setup(){
  Serial.begin(9600); 
  node.nodeInit("ABCDABCDABCDABCD", 99, RF12_915MHZ);
}

void loop(){
  unsigned long time = node.nodeLoop();
}
