#include <dht11.h>
#include <RFM12B.h>
#include <EEPROMIO.h>
#include <CommandHandler.h>
#include <EventQueue.h>
#include <RFM12bNode.h>
#define HUMIDITY_PIN      3  //Pin for humidity sensor connection

dht11 DHT11;

void setup(){
  Serial.begin(9600);
  DHT11.attach(HUMIDITY_PIN);
  node.nodeInit("ABCDABCDABCDABCD", 99, RF12_915MHZ);
 
  node.handler.registerDataSender(
  "humidity", 
  handleHumidityRequest, 
  "hum,rand");
  
  node.handler.registerDataSender(
  "temp", 
  handleTemperatureRequest, 
  "temp,rand");
  
  node.handler.registerCommandHandler(
  "printNum", handlePrintCommand, "num");
}

void loop(){
  node.nodeLoop(60000);
  node.queue.addQueueEvent(sendHumidity,0,0);
  node.queue.addQueueEvent(sendTemp,0,0);
}

int handlePrintCommand(char* message, int length){
  node.handler.setMessage(message, length);
  node.handler.getNextMessageField();
  char* num = node.handler.getNextMessageField();
  Serial.println(num);
  return 1;
}

int handleHumidityRequest(char* message, int length){
  node.queue.addQueueEvent(sendHumidity,0,0);
  return 1;
}

int handleTemperatureRequest(char* message, int length){
  node.queue.addQueueEvent(sendTemp,0,0);
  return 1;
}

void sendHumidity(unsigned int noValues[]){
  DHT11.read();
  int rand = random(1,20);
  int mssgSize = sprintf(node.getBuffer(), 
   "humidity|hum:%i;rand:%i",DHT11.humidity,rand);
   node.sendToGateway(mssgSize);
}

void sendTemp(unsigned int noValues[]){
  DHT11.read();
  int rand = random(21,100);
  int mssgSize = sprintf(node.getBuffer(), 
    "temp|temp:%i;rand:%i",DHT11.temperature,rand);
  node.sendToGateway(mssgSize);
}

