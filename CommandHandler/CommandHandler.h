#ifndef COMMAND_HANDLER_h
#define COMMAND_HANDLER_h

#include <inttypes.h>
#include <Arduino.h> 

//#define STRSIZE(x) ((sizeof(x)/sizeof(char))-1)
#define RECEIVE 0
#define SEND 1
#define NONE 2
#define PROTOCOL_MESSAGE_NUM 7
#define COMMAND_NUM 15
#define COMMAND_MAX_INDEX PROTOCOL_MESSAGE_NUM + COMMAND_NUM

typedef struct command_struct{
  int(*handler)(char*,int);
  char* name;
  char* fieldList;
  byte nameLen;
  byte type;
} Command;
 
class CommandHandler{

	private:
	Command commands[COMMAND_MAX_INDEX+1];	
	char* message;
	int messageLength;
	int messageFieldIndex;
	boolean hasNextField;

	public:
	int commandIndex;
	CommandHandler();
	int registerCommandHandler(char* command, int(*CommandHandler)(char*,int), char* fields);
	int registerDataSender(char* command, int(*CommandHandler)(char*,int), char* fields);
	int handleCommand(volatile uint8_t* data, volatile uint8_t length);
	
	void setMessage(char* mssg, int length);
	bool messageHasNext();
	char* getNextMessageField();
	char* getMessageRemainder();
	int getMessageRemainderLen();
	Command getCommand(char* cmd);
	int getCommandIndex(char* cmd);
	Command getCommandAt(int index);
	static int nullHandler(char* message, int length);
	
	
};

#endif
