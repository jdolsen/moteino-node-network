#ifndef MESSAGE_HANDLER_H
#define MESSAGE_HANDLER_H

typedef struct message_struct{
	int current;
	char indexes[30];
	char mssgBuffer[200];
}Message;

typedef struct message_element{
	char* field;
	char* value;
}MessageElement;

class MessageHandler{
	private:
	
	Message mssg;
	MessageElement element;
	
	public:
	MessageHandler();
	bool parseMessageString(char* mssgStr);
	MessageElement getNextMssgElement();

};

#endif