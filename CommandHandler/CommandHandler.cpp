
#include "CommandHandler.h"

CommandHandler::CommandHandler(){
	commandIndex=0;
	//set the null command (will be returned if a request to find a command fails)
	commands[COMMAND_MAX_INDEX].name = NULL;
	commands[COMMAND_MAX_INDEX].handler = nullHandler;
	commands[COMMAND_MAX_INDEX].nameLen = 0;
	commands[COMMAND_MAX_INDEX].fieldList = NULL;
	commands[COMMAND_MAX_INDEX].type = NONE;
}

//=============================================================================================

int CommandHandler::registerCommandHandler(char* command, int(*CommandHandler)(char*,int), char* fields){

	  if(commandIndex == COMMAND_MAX_INDEX){
		  return 0; 
	  }
	  commands[commandIndex].name = command;
	  commands[commandIndex].handler = CommandHandler;
	  commands[commandIndex].nameLen = strlen(command);
	  commands[commandIndex].fieldList = fields;
	  commands[commandIndex].type = RECEIVE;
	  commandIndex++;
	  return 1;
}

//=============================================================================================

int CommandHandler::registerDataSender(char* command, int(*CommandHandler)(char*,int), char* fields){
	
	if(commandIndex == COMMAND_MAX_INDEX){
		  return 0;
	}
	
	commands[commandIndex].name = command;
	commands[commandIndex].handler = CommandHandler;
	commands[commandIndex].nameLen = strlen(command);
	commands[commandIndex].fieldList = fields;
	commands[commandIndex].type = SEND;
	commandIndex++;
	return 1;
}

//=============================================================================================

int CommandHandler::handleCommand(volatile uint8_t* data, volatile uint8_t length){
	byte cmdLen = 0;
	data[length]='\0'; //put a terminating character at the end of the data array
	                   //this means the data array should be longer than the length!
	
	//find the point where the command string ends (the first '|' char)
    for(byte i=0; i < length; i++){
		if((char)data[i]=='|'){	
			data[i]='\0';
			cmdLen = i;	
			break;			
		} 
	}
	
	//find the command handler for this command and execute it
	for(int i = 0; i < commandIndex; ++i){
		if(commands[i].nameLen == cmdLen){
			if(strcmp((char*)data, commands[i].name)==0){ 
			//Serial.print("Executing command ");
			//Serial.println((char*)data);
				//return the data to its original state	
				data[cmdLen] = '|';	
				return commands[i].handler((char*)&data[cmdLen+1],length-cmdLen-1);	
				//Serial.println("Command Executed");
			}
		}
	}
	//return the data to its original state	
	data[cmdLen] = '|';
	//TODO:  allow a default handler to be added
	return false;		  
}

//=============================================================================================

	void CommandHandler::setMessage(char* mssg, int length){
		message = mssg;
		message[length] = '\0';
		messageFieldIndex = 0;	
		messageLength = length;
		hasNextField = true;
	}
	
//=============================================================================================
	
	bool CommandHandler::messageHasNext(){
		return hasNextField;	
	}

//=============================================================================================
	
	char* CommandHandler::getNextMessageField(){
		int mssgStart = messageFieldIndex;
		hasNextField = false; //set false until the start of next field is found
		for(byte i=messageFieldIndex; i < messageLength; i++){
			if(message[i]==';'||message[i]==':'||message[i]=='|'||message[i]=='\0'){
				//found the start of the next message field
				message[i]='\0';
				messageFieldIndex = i+1;	
				hasNextField = true;	
				break;
			} 
		}
		return (char*)&message[mssgStart];
	}
	
//=============================================================================================
	
	char* CommandHandler::getMessageRemainder(){
		hasNextField=false;	
		return (char*)&message[messageFieldIndex];	
	}
	
//=============================================================================================
	
	int CommandHandler::getMessageRemainderLen(){	
		return messageLength-messageFieldIndex;
	}
	
//=============================================================================================
	
	Command CommandHandler::getCommand(char* cmd){
		return commands[getCommandIndex(cmd)];
	}
	
//=============================================================================================
	
	int CommandHandler::getCommandIndex(char* cmd){
		for(int i = 0; i < commandIndex; ++i){
			if(strcmp(cmd, commands[i].name)==0){ 
				return i;	
			}		
		}
		return COMMAND_MAX_INDEX;
	}	
	
//=============================================================================================
	
	Command CommandHandler::getCommandAt(int index){
		if(index > COMMAND_MAX_INDEX){
			return commands[COMMAND_MAX_INDEX];
		}
		return commands[index];
	}
		
//=============================================================================================
	
	int CommandHandler::nullHandler(char* message, int length){
		return 1;
	}
	

	