#ifndef EVENT_QUEUE_h
#define EVENT_QUEUE_h

#include <inttypes.h>
#include <Arduino.h> 
#include <stdarg.h>
#define NUM_QUEUE_ELEMENT_VALUES 2


typedef struct queueelementstruct {
  void (*function)(unsigned int values[]);
  unsigned int values[NUM_QUEUE_ELEMENT_VALUES];
  unsigned int delayMs;
  byte isValid;
} QueueElement;

typedef void (*QueueFunction)(unsigned int values[]);


class EventQueue{

	public: 
	int queueSize;
	QueueElement* elements;	
	//int tickTimeMs;

	public:
	EventQueue();
	void initEvents(QueueElement* array, const int& size);//, const int& clockTick);
	bool addQueueEvent(QueueFunction fun, unsigned int delayMs, int n_args, ...);
	void executeFirstValidEvent(unsigned int timeSinceLastMs);
	void executeAllEvents(unsigned int timeSinceLastMs);
	void clear();
};

#endif