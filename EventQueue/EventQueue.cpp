#include "EventQueue.h"

EventQueue::EventQueue(){
}

void EventQueue::initEvents(QueueElement* array, const int& size){
  elements = array;
  queueSize = size;

  for(int i = 0; i<size; ++i){
    array[i].isValid=false;
  } 
} 

//=====================================================================

bool EventQueue::addQueueEvent(QueueFunction fun, unsigned int delayMs, int n_args, ...){
  //add the event to the first open slot
  boolean isAvailable = false;
  for(int i = 0; i<queueSize; ++i){
    if(elements[i].isValid==false){	  
      elements[i].function = fun;
      elements[i].delayMs = delayMs;
      elements[i].isValid = 1;
	  
	  va_list ap;
	  va_start(ap, n_args); 
	  for(int j = 0; j < n_args && j <NUM_QUEUE_ELEMENT_VALUES; j++) {
        elements[i].values[j] = va_arg ( ap, unsigned int );
	  }
      va_end(ap);
	  
      isAvailable = true;
	  
      break;
    }//endif
  }//endfor
  
  return isAvailable;
}

//=====================================================================

void EventQueue::executeFirstValidEvent(unsigned int timeSinceLastMs){
  boolean executed = false;
  for(int i = 0; i<queueSize; ++i){
     //only execute valid events
     if(elements[i].isValid==1){
       //only execute the first event with an expired event time
       if(elements[i].delayMs <=0 && !executed){
	     executed = true;
         elements[i].function(elements[i].values);
         elements[i].isValid = 0; 
         
       }else{
          //decrement the wait time for all other valid events
		 if(elements[i].delayMs < timeSinceLastMs){
			elements[i].delayMs = 0;
		 }else{
			 elements[i].delayMs = elements[i].delayMs - timeSinceLastMs;
         }
       }//endif-else
     }//endif valid      
  }//endfor
}

//=====================================================================

void EventQueue::executeAllEvents(unsigned int timeSinceLastMs){
  for(int i = 0; i<queueSize; ++i){
     //only execute valid events with an expired event time
     if(elements[i].isValid==1){ 
       if(elements[i].delayMs <=0){
         elements[i].function(elements[i].values);
         elements[i].isValid = 0; 
       }else{
          //decrement the wait time for all other valid events
         elements[i].delayMs = elements[i].delayMs - timeSinceLastMs;
         if(elements[i].delayMs < 0){
           elements[i].delayMs=0;
         }
       }//endif-else
     }//endif valid      
  }//endfor
}

void EventQueue::clear(){
	for(int i = 0; i<queueSize; ++i){
		elements[i].isValid=0;
	}
}