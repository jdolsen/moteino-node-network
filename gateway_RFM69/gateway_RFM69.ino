// Simple serial pass through program
// It initializes the RFM12B radio with optional encryption and passes through any valid messages to the serial port
// felix@lowpowerlab.com
#include <CommandHandler.h>
#include <EEPROMIO.h>
#include <MessageHandler.h>
#include <SPI.h>
#include <RFM69.h>
#include <avr/sleep.h>
#include <EventQueue.h>
#include <MemoryFree.h>

// You will need to initialize the radio by telling it what ID it has and what network it's on
// The NodeID takes values from 1-127, 0 is reserved for sending broadcast messages (send to all nodes)
// The Network ID takes values from 0-255
// By default the SPI-SS line used is D10 on Atmega328. You can change it by calling .SetCS(pin) where pin can be {8,9,10}
#define NODEID         127  //network ID used for this unit
#define NETWORKID       99  //the network ID we are on
#define SERIAL_BAUD   9600
#define gwVersion  "GWA0.1"
#define serialNum  "A001-000001"
//#define FREE_MEM_REPORT

#define NUM_CONNECTIONS 100
#define CHECK_TIMEOUT 120000 //600000 //10 minutes in msec
#define CLOCK_TICK 5

#define NODE_STATUS_UNDEFINED 0
#define NODE_STATUS_CONNECTED 1
#define NODE_STATUS_PENDING 2
#define NODE_STATUS_DISCONNECTED 3
#define NODE_STATUS_OVERDUE 5
#define NODE_STATUS_EMPTY 10 //If the status is incremented to this point, the node has been
//disconnected for over an hour and we assume the node ID can be safely reassigned

//Received Commands
#define USB_STAT_REQ "CNREQ"//request for node connection status from the host
#define USB_NODE_ID_REASSIGN "NRID" //request to reassign node ids
#define USB_RAND_REQ "URREQ"
#define NODE_CONNECTION_REQ "CR" //node requests new connection
#define NODE_STATUS_REPORT "SR" //node reports status
#define NODE_RECONNECT_REQ "RR" //node requests reconnect
#define NODE_STARTUP_INFO_REPORT "SI" //a node sends info for GW startup
#define GW_AVAILABLE_REQUEST "AR" //a gateway is looking for an available network
#define GW_NETWORK_OCCUPIED "NOCC" //a gateway is already using this network

//Sent commands


//encryption is OPTIONAL
//to enable encryption you will need to:
// - provide a 16-byte encryption KEY (same on all nodes that talk encrypted)
// - to call .Encrypt(KEY) to start encrypting
// - to stop encrypting call .Encrypt(NULL)
char* KEY = "ABCDABCDABCDABCD";

// Need an instance of the Radio Module
RFM69 radio;
const int IN_SIZE = 256;
char inData[IN_SIZE];
char payload[200];
byte connectionStatus[NUM_CONNECTIONS];
byte queueOverflowFlag = 0;
unsigned int badCrcCount = 0;

/** For this gateway implementation, in order to save space and processor cycles, 
  * the node SN will just be the same as the node ID (allowing direct comparison 
  * of the sender ID with the sender SN.  This also ensures a unique SN...
  * Nodes will still be required to store an unsigned int as their SN (to conform to the
  * node-gateway standard.)
  */
//unsigned int nodeSNs[NUM_CONNECTIONS]; 

CommandHandler handler;
EventQueue queue;
unsigned long checkTime = 0;
//unsigned int tempSN = 0;
//unsigned int tempNode = 0;
//unsigned int newID = 0;
const int QSIZE=10;

QueueElement eventQueue[QSIZE];

//========================================================================================================

void setup()
{
  delay(2000);
  radio.initialize(RF69_915MHZ, NODEID, NETWORKID);
  radio.encrypt(KEY);      //comment this out to disable encryption
  Serial.begin(SERIAL_BAUD);
  Serial.println("GWI:Gateway is listening...");

  for(int i = 0; i < NUM_CONNECTIONS; ++i){
     connectionStatus[i]=NODE_STATUS_EMPTY; 
  }
  queue.initEvents(eventQueue, QSIZE);
  handler.registerCommandHandler(USB_STAT_REQ, handleUSBStatusRequest, NULL); //request for node connection status
  handler.registerCommandHandler(USB_NODE_ID_REASSIGN, handleUSBReassignNode, NULL); //request to reassign node ids
  handler.registerCommandHandler(NODE_CONNECTION_REQ, handleNodeConnectionRequest, NULL); //node requests new connection
  handler.registerCommandHandler(NODE_STATUS_REPORT, handleNodeStatusReport, NULL); //node reports status
  handler.registerCommandHandler(NODE_RECONNECT_REQ, handleNodeReconnectRequest, NULL); //node requests reconnect
  handler.registerCommandHandler(NODE_STARTUP_INFO_REPORT, handleNodeStartupInfo, NULL); //a node sends info for GW startup
  handler.registerCommandHandler(GW_AVAILABLE_REQUEST, handleNetworkAvailableRequest, NULL); //another GW checks if this network is being used
  handler.registerCommandHandler(GW_NETWORK_OCCUPIED, handleNetworkOccupiedResponse, NULL); //another GW is already using this network.  Choose another...
  handler.registerCommandHandler(USB_RAND_REQ, handleUSBRandomRequest, NULL); //another GW is already using this network.  Choose another...


  //TASK: handler for node alive
  //TASK: handle relay node events
  
  randomSeed(analogRead(A0)); 
  reportFreeMemory(1);
  
  //TASK: select a free network ID (one without another gateway on it)
  //issue a GWSU command in order to get network info at startup
  radio.send(0, "GWSU|", 5);
}

//========================================================================================================

void loop()
{  
  receiveWirelessNodeData(); 
  receiveAndHandleUsbCommand();
  queue.executeAllEvents(CLOCK_TICK);
    
  if(checkTime >= CHECK_TIMEOUT){
    //every 10 minutes, check node connections
    requestNodeStatusReports();
    checkTime = 0;
  }else{   
    checkTime+=CLOCK_TICK; 
  }
  delay(CLOCK_TICK);
}

//========================================================================================================
//  Message Receivers
//========================================================================================================

void receiveWirelessNodeData(){
  if (radio.receiveDone()){     
      //if (radio.CRCPass()){
        //received proper message

        //if the command is not recognized forward data to the host
        if(!handler.handleCommand(radio.DATA, radio.DATALEN)){
          reportFreeMemory(10);
          Serial.print(radio.SENDERID);Serial.print("|");Serial.print(NODEID);Serial.print("|");
          for (byte i = 0; i < radio.DATALEN; i++){ //can also use radio.GetDataLen() if you don't like pointers
              Serial.print((char)radio.DATA[i]);
          }
          Serial.println();
        }
        
        if (radio.ACKRequested()){
            radio.sendACK();
        }
                
      /*}else{
        //received bad message
        badCrcCount++;
        Serial.print("GWER:Bad CRC #: ");
        Serial.print(badCrcCount);
        //Serial.println("");
        for (byte i = 0; i < *radio.DataLen; i++){ //can also use radio.GetDataLen() if you don't like pointers
          Serial.print((char)radio.Data[i]);
        }
        Serial.println();
      }*/
      reportFreeMemory(11);
  }
}

//========================================================================================================

void receiveAndHandleUsbCommand(){
  //recieve command from computer via USB  
  if(Serial.available()>0){
    int numBytesReceived = Serial.readBytesUntil(0,inData, IN_SIZE-1);
    inData[numBytesReceived] = 0;
    
   //parse the command and handle it 
   handler.setMessage(inData, numBytesReceived); 
   int dest = atoi(handler.getNextMessageField());
   int length = handler.getMessageRemainderLen();
   char* message = handler.getMessageRemainder();
   if(dest == NODEID){//handle the command here
     Serial.print(dest);
     Serial.print("|");
     Serial.print(dest);
     Serial.println("|GWACK|val:1");
     if(!handler.handleCommand((volatile uint8_t*)message, length)){
       //command not recognized, do a default behavior
       Serial.print("GWI:CMD to Gtway not recognized ");
       Serial.println(message);
     }      
   }else{ //forward the message
     Serial.print("GWI:Forwarding command to node ");
     Serial.print(dest);
     Serial.print(" with message ");
     Serial.println(message);
     radio.send(dest, message, length);
     Serial.print(dest);
     Serial.print("|");
     Serial.print(dest);
     Serial.println("|GWACK|val:1");
   } 
   
   //acknowledge message sent to node (or handled)
   //wait 10ms before acknowledging
   //delay(10);

   
  }//end if
}//end

//========================================================================================================
//  Wireless Command Handlers
//========================================================================================================

int handleNodeConnectionRequest(char* message, int length){
  unsigned int tempSN = (unsigned int)atol(message);
  queue.addQueueEvent(sendNewNodeIdAssignment, 100,1, tempSN);
  reportFreeMemory(2);
  return 1;
}

//========================================================================================================

int handleNodeStatusReport(char* message, int length){
  //A node will only report its status once when the GW 
  //sets its status to pending and requests an update.
  unsigned int nodeNum = (unsigned int)radio.SENDERID;
  handler.setMessage(message, length);
  handler.getNextMessageField();//strip the field label so the SN can be read in
  unsigned int reportedSN = (unsigned int)atoi(handler.getNextMessageField());
  if(reportedSN != nodeNum){
    //This gateway assigns the nodeNum as the SN for a properly connected node
    // therefore, this report is not from a properly connected node. This can happen
    // if the GW requests a status report before the node sends out its request for ID
    // send an newID IDA to ID=0 with the reportedSN in order to properly connect it
    //Multiple reports that have the same SN will end up assigned the same ID temporarily
    // and will be dealt with when the GW detects the problem (in this method -see below- 
    // or as a result of the node status reports).
    queue.addQueueEvent(sendNewNodeIdAssignment, 0, 1, reportedSN);
    return 1;
  }
  if(connectionStatus[nodeNum]==NODE_STATUS_CONNECTED){
    //A node using this ID and SN already reported its connection 
    //(and transitioned status from pending to connected)
    //send an IDA to the nodeID with newID=0 so that both nodes are set to "disconnected"
    //they will wait a random interval before attempting to get a new ID. 
    queue.addQueueEvent(sendNodeDisconnectCommand, 0, 1, nodeNum);
  }else{
    //connectionStatus[nodeNum]=NODE_STATUS_CONNECTED; 
    changeNodeStatus(nodeNum, NODE_STATUS_CONNECTED); 
  }   
  reportFreeMemory(4);
  return 1;
}

//========================================================================================================

int handleNodeStartupInfo(char* message, int length){
  unsigned int nodeNum = (unsigned int)radio.SENDERID;
  unsigned int reportedSN = (unsigned int)atoi(message);
  if(nodeNum!=reportedSN){
    queue.addQueueEvent(sendNewNodeIdAssignment, 0, 1, reportedSN);
    return 1;  
  }
  
  if(connectionStatus[nodeNum]==NODE_STATUS_CONNECTED){
   //This node was previously assigned, there is a duplicate node on the same ID
   queue.addQueueEvent(sendNodeDisconnectCommand, 0, 1, nodeNum); 
  }else{
    //this node has been properly assigned. 
    //connectionStatus[nodeNum]=NODE_STATUS_CONNECTED;   
    changeNodeStatus(nodeNum, NODE_STATUS_CONNECTED);
  }
  return 1;
}

//TODO: have nodes monitor the IDA requests and report a duplicate!! (easiest way to detect...)
 
//========================================================================================================

int handleNodeReconnectRequest(char* message, int length){
  
  handler.setMessage(message, length); 
  unsigned int nodeNum = (unsigned int)radio.SENDERID;
  unsigned int tempSN = (unsigned int)atoi(handler.getNextMessageField());
  //tempNode = (unsigned int)atoi(handler.getNextMessageField());
  
  //check that the reported serial number matches the SN assigned to the sending node
  //TODO:  something wrong here... (this will always be true, even for 
  if(tempSN == nodeNum && 
    connectionStatus[nodeNum] != NODE_STATUS_CONNECTED && //ensure there isn't a node already connected
    connectionStatus[nodeNum] != NODE_STATUS_PENDING ){   
    //if it does, confirm the reconnect by sending an IDA with the old nId and SN
    queue.addQueueEvent(sendNodeIdApproval, 0, 2, nodeNum, tempSN);
  }else{
    //if not, send an IDA with a new nId and SN
    queue.addQueueEvent(sendNewNodeIdAssignment, 0, 1, tempSN); 
  }
  return 1;
}

//========================================================================================================

int handleNetworkAvailableRequest(char* message, int length){
  //if another gateway wants to use this network, tell them it is currently occupied 
  queue.addQueueEvent(sendNetworkOccupied, 0,0);
  return 1;
}

//========================================================================================================

int handleNetworkOccupiedResponse(char* message, int length){
  //increment the network number to check
  //if the net number is >255, 
  //send a network available request
  return 1;
}

//========================================================================================================
// Wireless Senders
//========================================================================================================

void sendNewNodeIdAssignment(unsigned int values[]){
  int newId = getFirstAvailableNodeId();
  if(newId >0){
    //connectionStatus[newId] = NODE_STATUS_CONNECTED;
    changeNodeStatus(newId, NODE_STATUS_CONNECTED);
    //unsigned int nodeSN = getUniqueNodeSN();//0;//analogRead(A0);//getUniqueSN();random(2000000000L);//
    int mSize = sprintf(payload, "IDA|%u;%u;%u", values[0], newId, newId);//tempSN, nodeSN, newId);
    Serial.println("GWI: Sending IDA mssg");
    radio.send(0, payload, mSize);
    reportFreeMemory(3);
  }else{
    Serial.println("GWI: All IDs used"); 
  }
}

//========================================================================================================

void sendNodeIdApproval(unsigned int values[]){ 
    unsigned int nodeNum = values[0];
    unsigned int nodeSN = values[1];
    //connectionStatus[nodeNum] = NODE_STATUS_CONNECTED;
    changeNodeStatus(nodeNum, NODE_STATUS_CONNECTED);
    int mSize = sprintf(payload, "IDA|%u;%u;%u", nodeSN, nodeNum, nodeNum);//tempSN, nodeSN, newId);
    Serial.println("GWI: Sending IDA mssg");
    radio.send(0, payload, mSize);
    reportFreeMemory(3);
}

//========================================================================================================

void sendNetworkOccupied(unsigned int values[]){
  int mSize = sprintf(payload, "NOCC|");
  radio.send(0, payload, mSize);
}

//========================================================================================================

void sendNodeDisconnectCommand(unsigned int values[]){//Just an ID assignment to 0
  unsigned int oldId = values[0];
  //unsigned int newId = values[1];
  
  //int mSize = sprintf(payload, "DC|");//tempSN, nodeSN, newId);
  int mSize = sprintf(payload, "IDA|%u;0;0", oldId);
  Serial.println("GWI: Sending disconnect command mssg");
  radio.send(oldId, payload, mSize);
  changeNodeStatus(oldId, NODE_STATUS_DISCONNECTED);
  //connectionStatus[oldId] = NODE_STATUS_DISCONNECTED; //disconnect is intended to 
                                                        //be a temporary situation.  
  //TODO:  Nothing is done here...
  //queue.addQueueEvent(sendNodeIDReassignment, 200, 2, oldId, newId); //assign the node                                                
}

//========================================================================================================

void sendNodeDisconnectCommandWithReassign(unsigned int values[]){
  unsigned int oldId = values[0];
  unsigned int newId = values[1];
  
  //int mSize = sprintf(payload, "DC|");//tempSN, nodeSN, newId);
  int mSize = sprintf(payload, "IDA|%u;0;0", newId);//oldId); //send disconnect to new id
  Serial.println("GWI: Sending disconnect command mssg");
  radio.send(newId, payload, mSize);
  changeNodeStatus(newId, NODE_STATUS_DISCONNECTED);
  //connectionStatus[newId] = NODE_STATUS_DISCONNECTED; //disconnect is intended to 
                                                        //be a temporary situation.  
  //TODO:  this may cause a problem with the node state
  queue.addQueueEvent(sendNodeIDReassignment, 200, 2, oldId, newId); //assign the node                                                
}

//========================================================================================================
// USB command handlers
//========================================================================================================

int handleUSBStatusRequest(char* message, int length){
  Serial.print("GWS|Net:");
  Serial.print(NETWORKID);
  //Serial.print(";Ver:");
  //Serial.print(gwVersion);
  //Serial.print(";SN:");
  //Serial.print(serialNum);
  Serial.print(";Nodes:");
  for(int i = 1; i< NUM_CONNECTIONS; ++i){
   Serial.print(connectionStatus[i]);
   Serial.print(',');
  }
  Serial.println("");
  reportFreeMemory(5);
  return 1;
}

//========================================================================================================

int handleUSBReassignNode(char*message, int length){
  //Send a disconnect command if the new node id is currently connected and
  // send an IDA message to the requested node with the new node id
  handler.setMessage(message, length); 
  unsigned int oldId = (unsigned int)atoi(handler.getNextMessageField()); //old nID
  unsigned int newId = (unsigned int)atoi(handler.getNextMessageField());//new nID
  if(oldId == newId){return 1;}
  Serial.print("GWI:Received reassign request for node: ");
  Serial.print(oldId);
  Serial.print(" to node: ");
  Serial.println(newId);
  if(connectionStatus[newId]==NODE_STATUS_CONNECTED || connectionStatus[newId]==NODE_STATUS_PENDING){
    Serial.println("Sending disconnect with reassign");
    queue.addQueueEvent(sendNodeDisconnectCommandWithReassign, 0, 2, oldId, newId);
    //queue.addQueueEvent(sendNodeIDReassignment, 200, 2, oldId, newId); //assign the node
  }else{
    Serial.println("Sending node reassignment only");
    queue.addQueueEvent(sendNodeIDReassignment, 0, 2, oldId, newId); //assign the node
  }
  return 1;
}

//========================================================================================================

int handleUSBRandomRequest(char*message, int length){
  Serial.print("GWI:");
  for(int i = 0; i<100; ++i){
    Serial.print(random(601));
    Serial.print(",");
  }
  Serial.println(" ");
  return 1;
}

//========================================================================================================
// USB Command Senders
//========================================================================================================

void sendNodeIDReassignment(unsigned int values[]){
  //set old node id to empty
  //set new node id to connected

  unsigned int oldId = values[0];
  unsigned int newId = values[1];
  //connectionStatus[oldId] = NODE_STATUS_EMPTY;
  //connectionStatus[newId] = NODE_STATUS_CONNECTED; 
  changeNodeStatus(oldId, NODE_STATUS_EMPTY);
  changeNodeStatus(newId, NODE_STATUS_CONNECTED);
  
  //send an IDA to the node with its old SN (same as it's old node ID) and the new SN and ID 
  //(which are also the same)                  old SN,  newSN, newID
  int mSize = sprintf(payload, "IDA|%u;%u;%u", oldId,newId,newId);
  Serial.println("GWI: Sending IDA mssg for reassignment");
  radio.send(oldId, payload, mSize);
}

//========================================================================================================
// node management
//========================================================================================================

void requestNodeStatusReports(){
  //Any node that has a connection status above pending is incrementally 
  //downgraded until it reaches empty currently connected 
  //nodes are set to pending until they respond with status
  Serial.println("GWI:Checking node connections");
  for(int i = 0; i < NUM_CONNECTIONS; ++i){
     if(connectionStatus[i] < NODE_STATUS_EMPTY){
       changeNodeStatus(i, connectionStatus[i]+1);
       //connectionStatus[i]+=1; 
     }
  } 
  
  //send a global broadcast requesting node connection status
  //each node will wait a random amount of time (up to 30 secs) and respond
  //the gateway will store the updated status
  radio.send(0, "SREQ|", 5);
  reportFreeMemory(6);
}

//========================================================================================================

void changeNodeStatus(int nodeNum, int newState){
  int oldState = connectionStatus[nodeNum];
  connectionStatus[nodeNum] = newState;
 
  if(newState >= NODE_STATUS_DISCONNECTED){
    //send disconnected event
    Serial.print("NDE:");
    nodeStatusReportMessage(nodeNum, oldState, newState);
  }else if(oldState >= NODE_STATUS_DISCONNECTED && newState < NODE_STATUS_PENDING){
    //send connected event
   
    //Don't send information status change for pending/connected states
    //this info changes every time the gateway requests a status update from the nodes. 
    //the nodes are still considered connected during that state change.
    //if(oldState < NODE_STATUS_DISCONNECTED && newState < NODE_STATUS_DISCONNECTED){
      Serial.print("NCE:"); 
      nodeStatusReportMessage(nodeNum, oldState, newState);  
   }
}

//TODO: add message queuing.  And, instead of requesting status from all nodes all at once,
//periodically request status from connected nodes only

//========================================================================================================

void nodeStatusReportMessage(int nodeNum, int oldState, int newState){
  Serial.print(nodeNum);
  Serial.print("|OS:");
  Serial.print(oldState);
  Serial.print(";NS:");
  Serial.println(newState);
  
}


//========================================================================================================
//  Utility Functions
//========================================================================================================

int getFirstAvailableNodeId(){
   reportFreeMemory(7);
   for(int i = 1; i < NUM_CONNECTIONS; ++i){
     if(connectionStatus[i] == NODE_STATUS_EMPTY){      
       return i;
     }
   }  
   return 0;  
}

//========================================================================================================

// returns first-empty or first-and-most-overdue node
int getMostOverdueConnectionNode(){
   int mostOverdueConnection = 0;
   for(int i = 1; i < NUM_CONNECTIONS; ++i){
     int conStatus = connectionStatus[i];
     if(conStatus >= NODE_STATUS_OVERDUE && 
       conStatus > mostOverdueConnection){
        mostOverdueConnection = i;
     }
   } 
   return mostOverdueConnection;   
}

//========================================================================================================

/*unsigned int getUniqueNodeSN(){
  reportFreeMemory(8);
  while(1){
    unsigned int randSN = (unsigned int)random(1, 65535);
    if(isUniqueSN(randSN)==1){
      return randSN;
    }
  }//end while 
}

//========================================================================================================

int isUniqueSN(unsigned int sn){
  reportFreeMemory(9);
  for(int i = 0; i < NUM_CONNECTIONS; ++i){
    if(sn == nodeSNs[i]){
      return 0;
    }
  }
  return 1;
}*/

//========================================================================================================

void reportFreeMemory(int index){
  #ifdef FREE_MEM_REPORT
  Serial.print("GWI:FreeMemory at ");
  Serial.print(index);
  Serial.print(" = ");
  Serial.println(freeMemory());
  #endif
}



