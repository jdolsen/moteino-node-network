
#include <RFM12bNode.h>

//STATIC INITIALIZATION
	CommandHandler nodeHandler;
	EventQueue nodeQueue;
	RFM12B nodeRadio;
	
	char RFM12bNode::payload[MESSAGE_SIZE] = {0};
	uint8_t* RFM12bNode::nodeKey=(uint8_t*)"A";
	CommandHandler RFM12bNode::handler=nodeHandler;
	EventQueue RFM12bNode::queue=nodeQueue;
	RFM12B RFM12bNode::radio=nodeRadio;
	long RFM12bNode::total=0;	
	byte RFM12bNode::nodeid = 3;
	byte RFM12bNode::nodeState = NODE_INIT;	
	unsigned int RFM12bNode::randTime=0;
	unsigned int RFM12bNode::nodeSN=0;
	unsigned int RFM12bNode::badCrcCount=0;
	unsigned int RFM12bNode::netId=0;
	
	volatile unsigned long nodeTimerUs = 0UL;//holds the time for the full node run
	volatile unsigned long nodeTickTimerUs = 0UL; //holds the time for a single node tick
	volatile unsigned long nodeACKTimer = 0UL; //holds the time while waiting for an ack response
	unsigned int nodeMessageCounter = 0; //holds the number of messages this node has sent
	unsigned int numMessageSendFailures = 0;
	
	char nodeName[NAMESIZE+1];
//========================================================================================================
//Class methods
//========================================================================================================	
	
	byte RFM12bNode::getNodeState(){
		return nodeState;
	}
	
//========================================================================================================

	char* RFM12bNode::getBuffer(){
		return payload;
	}
	
//========================================================================================================
	
	void RFM12bNode::nodeInit(const char* key, unsigned int nId, uint8_t band){

	  nodeKey = (uint8_t*)key;
	  netId = nId;

	  //load the node name
	  EEPROMIO.loadString(nodeName, (int)NAMESIZE, 0); 
	  nodeName[NAMESIZE]='\0';//ensure the node name array doesn't overrun if no name has been set
	  
	  //FFFV: find the network (feature for future version)
	  radio.Initialize(nodeid, band, netId);
	  radio.Encrypt(nodeKey);
	  queue.initEvents(eventQueue, QSIZE);//, NODE_TICK);

	  //Gateway commands
	  handler.registerCommandHandler("IDA", handleIDAssignment, NULL);
	  handler.registerCommandHandler("SREQ", handleStatusCommand, NULL); 
	  handler.registerCommandHandler("GWSU", handleGatewayStartup, NULL); 
	  //handler.registerCommandHandler("DC", handleDisconnectCommand); 
	  
	  //Host commands
	  handler.registerCommandHandler("RCMD", handleCMDListCommand, NULL);
	  handler.registerCommandHandler("FRQ", handleFRQCommand, NULL);
	  handler.registerCommandHandler("RNM", handleNameReqCommand, NULL); 
	  handler.registerCommandHandler("SNM", handleSetNameCommand, NULL);
	  
	  
	  randomSeed(analogRead(A0)); 
	  
	  //set timer2 interrupt at 100kHz (10us)
		  cli();
		  TCCR2A = 0;// set entire TCCR1A register to 0
		  TCCR2B = 0;// same for TCCR1B
		  TCNT2  = 0;//initialize counter value to 0
		  // set compare match register for 1kHz increments
		  OCR2A = 19;// = (16*10^6) / (1000*64) - 1 (must be <256)	  
		  TCCR2A |= (1 << WGM21); // turn on CTC mode	  
		  TCCR2B |= (1 << CS21); // Set CS21 bit for 8 prescaler  
		  TIMSK2 |= (1 << OCIE2A);// enable timer compare interrupt
		  sei();
	  nodeState = NODE_START;
	  
	  #ifdef NODE_PRINT
	  Serial.println("Initialized");
	  #endif
	}
	
//========================================================================================================
//  Utility Functions
//========================================================================================================

void RFM12bNode::startTimer(){
  nodeTimerUs=0UL; 
}

void RFM12bNode::startNodeTickTimer(){
	nodeTickTimerUs=0UL;
}

//========================================================================================================
	
ISR(TIMER2_COMPA_vect){//timer2 interrupt 1kHz toggles pin 9 at 2Hz
	nodeTimerUs+=NODE_TIMER_US;
	nodeTickTimerUs+=NODE_TIMER_US;
	nodeACKTimer+=NODE_TIMER_US;
}	
	
//========================================================================================================
//delayMs is the amount of time the nodeLoop function can run until it returns
unsigned long RFM12bNode::nodeLoop(unsigned long dMs){
	unsigned long delayUs = 1000UL * dMs;
	
	//start the timer for this run
	startTimer();
	
	//run until a break is signalled
	while(1){
		
		//timer was reset at start of last tick
		//wait until timer counts to the node_tick_time before ticking again
		//will tick immediately if the time has already expired			
		while(nodeTickTimerUs < NODE_TICK_US);
		while(nodeTickTimerUs < NODE_TICK_US);
		while(nodeTickTimerUs < NODE_TICK_US);//this is done 3 times since the comparison often allows early exit

		unsigned long elapsedTimeUs = nodeTickTimerUs; //time spent outside the node function
		startNodeTickTimer(); //start the timer at the beginning of this tick
		
		//run node functions
		receiveWirelessCommand();
		queue.executeFirstValidEvent(elapsedTimeUs/1000UL);	
	  
		switch(nodeState){
			case NODE_INIT:
			  //do nothing
			break;
			case NODE_START:
			  nodeStart();
			break;
			//case NODE_CONNECT:
			  //do nothing
			//  nodeState=NODE_CONNECTING;
			//break;			
			case NODE_CONNECTING:
			  nodeConnecting();
			break;
			//case NODE_CONNECTED:
			//  nodeConnected();
			//break;
			case NODE_RUNNING:
			  nodeRunning();			  
			  break;
			case NODE_DISCONNECTING:
			  nodeDisconnecting();
			break;  			  
			case NODE_DISCONNECTED:
			  nodeDisconnected();
			break;
			default:
			  //if we ever get here, just reset the state
			  nodeState = NODE_START;
			break;
		}
		
		long remainingTimeUs = delayUs-nodeTimerUs;
		
		//node functions complete.  determine if another tick should be executed.
		//if remaining time is less than node_tick_time, wait for remaining
		//time to expire and exit	
		if(remainingTimeUs < NODE_TICK_US || remainingTimeUs < 0){
			//wait the remaining time and break
			while(nodeTimerUs < delayUs);
			while(nodeTimerUs < delayUs);
			while(nodeTimerUs < delayUs);//this is done 3 times since the comparison exits early sometimes
			break;
		}else{
			//run again
		}
	  }//end while
	  
	  //return total amount of time spent in usec
	  return nodeTimerUs;
}
	
//========================================================================================================

	boolean RFM12bNode::sendToGateway(int size, int ackReq){
		//Before sending to the gateway, always check to see if a message was 
		  //received (a call to receiveWirelessCommand receives, parses and handles  
		  //the message and queues any message senders) This is necessary if there 
		  //was an ack timeout or a long time spent out of the node loop.
				
		nodeMessageCounter++;
		Serial.println("Sending Message");
		//if requesting an ack this time...
		if(ackReq !=ACK_REQUEST_NEVER &&
		   (nodeMessageCounter % MESSAGES_PER_ACK == 0 || ackReq == ACK_REQUEST_ALWAYS)){
		   
			//try sending the message until max retries is reached or the send is successful
			boolean messageSuccess = false;
			byte numRetries = 0;
			while(numRetries < MESSAGE_RETRIES && messageSuccess==false){	
				numRetries++;				
				messageSuccess = true;
				
				receiveWirelessCommand(); 
				if(nodeState!=NODE_RUNNING){ //don't send if the node isn't running
					return false;
				}
				radio.Send(GATEWAYID, payload, size, true);				
				
				//waits up to ACK_TIMEOUT seconds for an ack
				nodeACKTimer = 0;
				while(!radio.ACKReceived(GATEWAYID)){
					//if time expires, set message success false and break to retry the message again
					if(nodeACKTimer >= (ACK_TIMEOUT*1000UL)){ 
						messageSuccess = false;
						numMessageSendFailures++;
						Serial.println("Message send failure");
						break;
					}		
				}//end ack wait loop
			}//end retry while loop

			return messageSuccess;			
			
		}else{//no ack requested
			receiveWirelessCommand();
			if(nodeState!=NODE_RUNNING){ //don't send if the node isn't running
				return false;
			}
			radio.Send(GATEWAYID, payload, size);
			return true;
		}	
		  
		//FFFV: determine connection health - based on message failures and act accordingly
		//FFFV:  (feature for future version) Adjust transmit power levels 
		    //(0 (max) through 7 (min)) periodically set the power to one value 
			//lower and initiate a ping exchange similarly, if power level is not 
			//max and a disconnect is detected (or there are a lot of bad crcs) 
			//adjust power to see if aconnection can be re-established
	}
	
//========================================================================================================
// Node state methods
//========================================================================================================

	void RFM12bNode::nodeStart(){
	  //start the connection process by clearing the radio queue and requesting a node ID
	  queue.clear();
	  randTime = random(601L)*50;//a time to wait between zero and 30seconds (30000ms) in increments of 50ms
	  nodeSN = (unsigned int)random(500, 65535);//an id between 500 and 65535 (close to max positive size of int)
	  // 500 is a minimum value to prevent collision with Gateway assigned SNs.
	  //broadcast message to network
	  int mSize = sprintf(payload, "CR|%u", nodeSN);//connection request
	  radio.Send(0, payload, mSize);
	  nodeState = NODE_CONNECTING;
	  total = 0;
	  #ifdef NODE_PRINT
	  Serial.println("Started");
	  #endif
	}

//========================================================================================================

	void RFM12bNode::nodeConnecting(){
	  //if the node has been in the connecting state for 1 minute, transition 
	  //back to node start to request an nID
	   if(total >= NODE_CONNECTING_TIMEOUT){
		nodeState = NODE_START;
	   }else{
	    total+=NODE_TICK;
	   }
	   #ifdef NODE_PRINT
	   Serial.println("Node Connecting");
	   #endif
	}

//========================================================================================================

	void RFM12bNode::nodeConnected(){
	  //handle any connection cleanup and initialization
	  //currently nothing to do here - move to running state
	  nodeState = NODE_RUNNING;
	  #ifdef NODE_PRINT
	  Serial.print("Connected using id: ");
	  Serial.println(nodeid);
	  #endif
	}

//========================================================================================================

	void RFM12bNode::nodeRunning(){
	  //Do everything else here	 
	}
	
//========================================================================================================

	void RFM12bNode::nodeDisconnecting(){
		#ifdef NODE_PRINT
		Serial.println("Disconnecting");		
		#endif
		nodeSN = (unsigned int)random(500, 65535);
		nodeid = 0;
		queue.clear();
		nodeState = NODE_DISCONNECTED;
	}	

//========================================================================================================

	void RFM12bNode::nodeDisconnected(){
		#ifdef NODE_PRINT
		Serial.println("Disconnected");		
		#endif
		//TODO: for now, set the node state to NODE_START this will clear the queue,  
		// generate a random SN and time interval, and send an IDR to the gateway
		nodeState = NODE_START;
	}
	
	
//========================================================================================================
//  Message Receivers
//========================================================================================================

void RFM12bNode::receiveWirelessCommand(){
   if (radio.ReceiveComplete()){
      if (radio.CRCPass()){        
          if (radio.ACKRequested()){
              radio.SendACK(); 
          } 
		  #ifdef NODE_PRINT
          Serial.println("Received radio message: ");
		  radio.Data[*radio.DataLen] = '\0';
		  Serial.println((char*)radio.Data);      
		  #endif
          handler.handleCommand(radio.Data, *radio.DataLen);
                                                      
      }else{
		#ifdef NODE_PRINT
		Serial.println("Received bad CRC"); 
		#endif
        badCrcCount++;
      }
  }
}

//========================================================================================================
//  Wireless Command Handlers
//========================================================================================================

 int RFM12bNode::handleStatusCommand(char* message, int length){
  #ifdef NODE_PRINT
  Serial.print("Sending status in: "); 
  Serial.println(randTime);
  #endif
  if(nodeState == NODE_RUNNING){// || nodeState == NODE_CONNECTED){
    boolean success = queue.addQueueEvent(sendStatus, randTime,0);
    randTime = random(601L)*50;//a time to wait between zero and 30seconds (30000ms) in increments of 50ms
  }else{
    //Received status request while not connected
  }
  return 1;
}

//========================================================================================================

 int RFM12bNode::handleIDAssignment(char*message, int length){
    #ifdef NODE_PRINT
	Serial.println("Received ID Assignment"); 
	#endif
  //ensure this is from the gateway node!
  if(radio.GetSender() == GATEWAYID){

    handler.setMessage(message, length);
    char* snField = handler.getNextMessageField(); //the current sn is the first field
	char* asgnSN = handler.getNextMessageField(); //the assigned SN is the next field
	char* nField = handler.getNextMessageField(); //the newly assigned ID is the next field	
	long sn = atol(snField); //SN of the destination node
	long nid = atol(nField); //newly assigned ID
	int nSN = atoi(asgnSN);  //newly assigned SN
	
	if(nodeState != NODE_CONNECTING && nid==nodeid){	
		//if this node is not trying to connect but the assigned Node ID
		//is the same as this one, then the gateway is duplicately assigning 
		//this node's id - a collision.
		// disconnect and attempt to reconnect.  It is likely the GW has 
		// not received several status reports from this node and has reassigned the Id.	
		nodeState = NODE_DISCONNECTING;
	}else if(sn == nodeSN){ //message is intended for this node - do ID assignment		
		nodeSN = nSN;
		nodeid = nid;
		#ifdef NODE_PRINT
		Serial.print("Assigned to: "); 
		Serial.println(nodeid);
		#endif
		if(nodeid == 0){		
			nodeState = NODE_DISCONNECTING; //if the assigned nId is 0, handle the disconnection
		}else{                
			radio.Initialize(nodeid, RF12_915MHZ, netId);
			radio.Encrypt(nodeKey); 
			nodeState = NODE_RUNNING;//NODE_CONNECTED;		
		}  		
	}else{
		//else message is not for this node
		#ifdef NODE_PRINT
		Serial.print("IDA not for this node: ");
		radio.Data[*radio.DataLen]='\0';
        Serial.println((char*)radio.Data); 
		#endif
	}
  }//end gateway if
  return 1;
}

//========================================================================================================

 int RFM12bNode::handleGatewayStartup(char* message, int length){
	#ifdef NODE_PRINT
	Serial.println("Received gateway startup"); 
	#endif
  boolean success = queue.addQueueEvent(sendStartupInfo, randTime,0);
  randTime = random(601L)*50;//a time to wait between zero and 30seconds (30000ms) in increments of 50ms
  return 1;
}

//========================================================================================================

int RFM12bNode::handleCMDListCommand(char* message, int length){
  #ifdef NODE_PRINT
  Serial.println("handling command list");
  #endif
  boolean success = queue.addQueueEvent(sendCommands, 0,0);

  return 1;
}

//========================================================================================================

int RFM12bNode::handleFRQCommand(char* message, int length){
  handler.setMessage(message, length);
  handler.getNextMessageField();
  char* val = handler.getNextMessageField();
  int index = handler.getCommandIndex(val);

  boolean success = queue.addQueueEvent(sendCommandFields, 0,1, index); 
  return 1;  
}

//========================================================================================================

int RFM12bNode::handleNameReqCommand(char* message, int length){
  boolean success = queue.addQueueEvent(sendName, 0,0);
  return 1;
}

//========================================================================================================

int RFM12bNode::handleSetNameCommand(char* message, int length){
  #ifdef NODE_PRINT
  Serial.print("handling set name with length: ");
  Serial.println(length);
  #endif
  
  handler.setMessage(message, length);
  char* name = handler.getNextMessageField();
  
  int len = length+1;//adding 1 to include the null character 
					 //for string termination
  if(len>NAMESIZE){
    len = NAMESIZE;
  }
    
  EEPROMIO.saveString(name, len, 0);
  EEPROMIO.write(len,'\0'); //save a null character to the end of the string 
  EEPROMIO.loadString(nodeName, NAMESIZE, 0);
  return 1;
}

//========================================================================================================

 /* int RFM12bNode::handleDisconnectCommand(char* message, int length){
  #ifdef NODE_PRINT
  Serial.println("Received disconnect command"); 
  #endif
  nodeid = 0;
  nodeSN = 0;  
  nodeState = NODE_DISCONNECTING;
  return true;
}*/

//========================================================================================================
// Wireless Senders
//========================================================================================================

 void RFM12bNode::sendStatus(unsigned int noValues[]){ 
  #ifdef NODE_PRINT
  Serial.println("Sending status");  
  #endif
  int mSize = sprintf(payload, "SR|NODESN:%u",nodeSN);
  radio.Send(GATEWAYID, payload, mSize);
}

//========================================================================================================

 void RFM12bNode::sendStartupInfo(unsigned int noValues[]){
  #ifdef NODE_PRINT
  Serial.println("Sending startup info"); 
  #endif
  int mSize = sprintf(payload, "SI|%u",nodeSN);
  radio.Send(GATEWAYID, payload, mSize);
}

//========================================================================================================

void RFM12bNode::sendCommands(unsigned int noValues[]){  
  #ifdef NODE_PRINT
  Serial.println("sending command list");
  #endif
  //for each command in the registered command list, check if it has fields
  //if it does, add it to the command list. Commands without a registered field
  //list are part of the protocol and need not be enumerated
  int numCommands = handler.commandIndex;
  
  //add receivers
  int mSize = sprintf(payload, "CF|");
  
  mSize += sprintf(payload+mSize, "cmds:");
  for(int i=0; i<numCommands; ++i){
    Command cmd = handler.getCommandAt(i);
    if(cmd.fieldList!=NULL && cmd.type==RECEIVE){
      mSize += sprintf(payload+mSize, "%s,", cmd.name);    
    } 
  }
  
  //add data senders
  mSize += sprintf(payload+mSize, ";snds:");
  for(int i=0; i<numCommands; ++i){
    Command cmd = handler.getCommandAt(i);
    if(cmd.fieldList!=NULL && cmd.type==SEND){
      mSize += sprintf(payload+mSize, "%s,", cmd.name);    
    } 
  }
  
  radio.Send(GATEWAYID, payload, mSize);
  
  //node.sendToGateway(mSize, ACK_REQUEST_ALWAYS);
}

//========================================================================================================

void RFM12bNode::sendCommandFields(unsigned int values[]){  
  int cmdIndex = values[0];
  Command cmd = handler.getCommandAt(cmdIndex);
  #ifdef NODE_PRINT
  Serial.println("sending command fields");
  #endif
  int mSize = sprintf(payload, "ACF|cmd:%s;f:%s", cmd.name ,cmd.fieldList); 
  radio.Send(GATEWAYID, payload, mSize);  
  //node.sendToGateway(mSize, ACK_REQUEST_ALWAYS);
}

//========================================================================================================

void RFM12bNode::sendName(unsigned int noValues[]){ 
  #ifdef NODE_PRINT
  Serial.println("sending name");
  #endif
  int mSize = sprintf(payload, "NAM|v:%s", nodeName);  
  radio.Send(GATEWAYID, payload, mSize);
  //node.sendToGateway(mSize, ACK_REQUEST_ALWAYS);
}

//========================================================================================================

RFM12bNode node;
