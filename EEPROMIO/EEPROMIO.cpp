
#include <avr/eeprom.h>
#include "Arduino.h"
#include "EEPROMIO.h"

//========================================================================================================

uint8_t EepromIoClass::read(int address){
	return eeprom_read_byte((uint8_t *) address);
}

//========================================================================================================

void EepromIoClass::write(int address, uint8_t value){
	eeprom_write_byte((uint8_t *) address, value);
}

//========================================================================================================

int EepromIoClass::saveString(char* str, int maxSize, int address){
	int i = 0;
	for(; i < maxSize; ++i){
		write(address+i, str[i]);
		if(str[i]=='\0'){
			++i;
			break;
		}	
	}
	
	if(i>=maxSize){
		//if the max size limit was reached, ensure the 
		//string is terminated with a null character
		write(address+i-1, 0);;
	}
	return i;
}

//========================================================================================================

int EepromIoClass::loadString(char* str, int maxSize, int address){
	int i = 0;
	for(; i < maxSize; ++i){
		str[i] = (char)read(address+i);
		if(str[i]=='\0'){
			++i;
			break;
		}
	}	
	
	if(i>=maxSize){
		//if the max size limit was reached, ensure the  
		//string is terminated with a null character
		str[maxSize]='\0';
	}	
	return i;
}


EepromIoClass EEPROMIO;
