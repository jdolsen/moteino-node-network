#ifndef EEPROMIO_H
#define EEPROMIO_H

#include <inttypes.h>

class EepromIoClass{ 
  public:
	uint8_t read(int);
    void write(int address, uint8_t value);	
	int saveString(char* str, int size, int address);
	int loadString(char* str, int size, int address);
	
	template <class T> int save(const T& value, int address)
	{
	   const byte* p = (const byte*)(const void*)&value;
	   int i;
	   for (i = 0; i < sizeof(value); i++){
		   write(address++, *p++);
	   }
	   return i;
	}

	//========================================================================================================

	template <class T> int load(T& value, int address)
	{
	   byte* p = (byte*)(void*)&value;
	   int i;
	   for (i = 0; i < sizeof(value); i++){
		   *p++ = read(address++);
	   }
	   return i;
	}
	
};

extern EepromIoClass EEPROMIO;


#endif