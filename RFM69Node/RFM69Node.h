#ifndef RFM69Node_H
#define RFM69Node_H

#include <SPI.h>
#include <RFM69.h>
#include <EventQueue.h>
#include <CommandHandler.h>
#include <EEPROMIO.h>

#define GATEWAYID   127  //the node ID we're sending to
#define ACK_TIMEOUT  500  // # of ms to wait for an ack before retrying the message
#define MESSAGES_PER_ACK 10 //request an ack once every 10 messages sent)
#define ACK_REQUEST_ALWAYS 0
#define ACK_REQUEST_NEVER 1
#define ACK_REQUEST_PERIODIC 2
#define MESSAGE_RETRIES 3 //not to exceed 127!!!
#define NODE_CONNECTING_TIMEOUT 60000 //# of ms to wait while trying to establish a 
										//connection to the gateway

#define NODE_INIT 0
#define NODE_START 1
//#define NODE_CONNECT 2
#define NODE_CONNECTING 3
//#define NODE_CONNECTED 4
#define NODE_RUNNING 5
#define NODE_DISCONNECTING 6
#define NODE_DISCONNECTED 7
#define NODE_PRINT

const unsigned long NODE_TICK = 100L; //msec
const unsigned long NODE_TICK_US = 100000L;
const unsigned long NODE_TIMER_US = 10L;

const int QSIZE=8;
const int MESSAGE_SIZE=61;

const int NAMESIZE=12+1;//add 1 for the terminating character

	 
class RFM69Node{
	public:
	static CommandHandler handler;
	static EventQueue queue;
		
	private:
	QueueElement eventQueue[QSIZE];
	
	static RFM69 radio;
	static char payload[MESSAGE_SIZE];
	static char* nodeKey;
	static long total;	
	static byte nodeid;
	static byte nodeState;
	static unsigned int randTime;
	static unsigned int nodeSN;
	static unsigned int badCrcCount;
	static unsigned int netId;
	
	//========================================================================================================
	public:
	byte getNodeState();
	//void addCommandReceiver();
	//void setRunMethod(QueueFunction funct);	
	char* getBuffer();
	void nodeInit(char* key, unsigned int netId, uint8_t band);
	unsigned long nodeLoop(unsigned long delayMs=10000);
	boolean sendToGateway(int size, int ackReq=ACK_REQUEST_PERIODIC);
	//========================================================================================================

	private:

	//Node State Methods
	void nodeStart();
	void nodeConnecting();
	void nodeConnected();
	void nodeRunning();
	void nodeDisconnecting();
	void nodeDisconnected();
	
	//Message Receivers
	void receiveWirelessCommand();
	
	//Wireless Command Handlers (gateway)
	static int handleStatusCommand(char* message, int length);
	static int handleIDAssignment(char*message, int length);
	static int handleGatewayStartup(char* message, int length);
	//static int handleDisconnectCommand(char* message, int length);
	
	//Wireless Command Handlers (host)
	static int handleCMDListCommand(char* message, int length);
	static int handleFRQCommand(char* message, int length);
	static int handleNameReqCommand(char* message, int length);
	static int handleSetNameCommand(char* message, int length);
	
	//Wireless Senders (gateway)
	static void sendStatus(unsigned int noValues[]);
	static void sendStartupInfo(unsigned int noValues[]);	
	
	//Wireless Senders (host)
	static void sendCommands(unsigned int noValues[]);
	static void sendCommandFields(unsigned int values[]);
	static void sendName(unsigned int noValues[]);

	//Utility Functions	
	static void startTimer();
	static void startNodeTickTimer();
	static void nodeWait();
};

extern RFM69Node node;


#endif