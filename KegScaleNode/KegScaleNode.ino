#include <RFM12B.h>
#include <EEPROMIO.h>
#include <CommandHandler.h>
#include <EventQueue.h>
#include <RFM12bNode.h>

double zero=0;
double full=1024;
int numMeasurements = 100;
char zeroStr[12];
char fullStr[12];
char valStr[12];
int count = 0;
double kgs = 0;
int numVals = 0;
int BASE_ADX;

void setup(){
  delay(3000);
  Serial.begin(9600);
  node.nodeInit("ABCDABCDABCDABCD", 99, RF12_915MHZ);
  BASE_ADX = NAMESIZE;
  BASE_ADX += EEPROMIO.load(zero, BASE_ADX);
  BASE_ADX += EEPROMIO.load(full, BASE_ADX);
  BASE_ADX += EEPROMIO.load(numVals, BASE_ADX);
  
  node.handler.registerDataSender(
    "wt", 
    handleWeightRequest, 
    "kgs,pctfull");
    
  node.handler.registerDataSender(
    "params", 
    handleParamsRequest, 
    "zro,ful");
    
  node.handler.registerCommandHandler(
  "setZro", zeroScale, "");
  
  node.handler.registerCommandHandler(
  "setFul", setKegFull, "");
  
  node.handler.registerCommandHandler(
  "sTblVal", setTableVal, "mv,kgs,num");
  
  node.handler.registerCommandHandler(
  "prtTbl", prtTbl, "");
  
  //send an initial value on startup
  node.queue.addQueueEvent(sendWeight,0,0);
}

void loop(){
  node.nodeLoop(60000);
  //every 5 minutes read the weight and check to see if it has
  //changed significantly (by a threshold amount (.4kgs)).  if so, make a report.  
  //Otherwise, do nothing.
  //TODO: account for temperature drift on the weight measurement
  if(count == 5){
    double value1 = measureAverage(numMeasurements);
    double newkgs = getWeightFromTable(value1);
    double diff = getPosDifference(kgs, newkgs);
    if(diff >=0.4){//a pint weighs about .57kg(UK) .55kg(US)
      kgs = newkgs;
      node.queue.addQueueEvent(sendWeight,0,0);
    }
    count = 0;  
  }else{
    count++;
  }
  
}

//============================================================

double getPosDifference(double a, double b){
  double diff = a-b;
  if(diff<0){
    diff=diff*-1;
  }
  return diff;
}

//============================================================

int handleWeightRequest(char* message, int length){
  node.queue.addQueueEvent(sendWeight,0,0);
  return 1;
}

int handleParamsRequest(char* message, int length){
  node.queue.addQueueEvent(sendParams,0,0);
  return 1;
}

int zeroScale(char* message, int length){
  zero = measureAverage(numMeasurements);
  EEPROMIO.save(zero, NAMESIZE);
}

int prtTbl(char* message, int length){

  Serial.println(numVals);
  double currentVal;
  double currentWt;
  int nextAdx = BASE_ADX;
  for(int i = 0; i<numVals; ++i){
   nextAdx += EEPROMIO.load(currentVal ,nextAdx);
   nextAdx += EEPROMIO.load(currentWt, nextAdx);
   Serial.print(i);
   Serial.print(" - ");
   Serial.print(currentVal);
   Serial.print(",");
   Serial.println(currentWt);
  }
}

int setKegFull(char* message, int length){
  full = measureAverage(numMeasurements);
  EEPROMIO.save(full, NAMESIZE + 4);
}

//============================================================

void sendWeight(unsigned int noValues[]){
  double value1 = measureAverage(numMeasurements);
  double kgs = getWeightFromTable(value1);
  Serial.print(value1);
  Serial.print("mv evaluates to: ");
  Serial.println(kgs);
  double percent = 100*(value1-zero)/(full-zero);
  dtostrf(kgs,0,4,valStr);
  dtostrf(percent,0,4,fullStr);
  int msgSize = sprintf(node.getBuffer(),"wt|kgs:%s;pctfull:%s",valStr,fullStr);
  node.sendToGateway(msgSize);
}

//============================================================

void sendParams(unsigned int noValues[]){
  dtostrf(zero,0,4,zeroStr);
  dtostrf(full,0,4,fullStr);
  int msgSize = sprintf(node.getBuffer(),"params|zro:%s;ful:%s",
    zeroStr,fullStr);
  node.sendToGateway(msgSize);
}

//============================================================

double measureAverage(int numSamples){
  int numLoops = numSamples;
  if(numLoops==0){
    numLoops=10;
  }
  double total = 0;
  for(int i = 0; i<numLoops; ++i){
   total+=analogRead(A1);
   delay(1);//delay 1ms 
  }
  return total/numLoops;
}

//============================================================

//get values from the lookup table until one is found greater than 
//the value passed in.  Use it and its predecessor to determine the weight
double getWeightFromTable(double val){
  double previousVal = zero;
  double previousWt = 0;
  double currentVal = zero;
  double currentWt = 0;
  int nextAdx = BASE_ADX;
  for(int i = 0; i<numVals; ++i){
   nextAdx += EEPROMIO.load(currentVal, nextAdx);
   nextAdx += EEPROMIO.load(currentWt, nextAdx);
   if(currentVal>=val){
     break; 
   }else{
     previousVal = currentVal;
     previousWt = currentWt;  
   }
  }//end for
  Serial.print(previousWt);
  Serial.print(",");
  Serial.print(currentWt);
  Serial.print(" - ");
  Serial.print(previousVal);
  Serial.print(",");
  Serial.print(currentVal);
  Serial.print(" - ");
  //find the slope between current and previous
  double slope = (currentWt-previousWt)/(currentVal-previousVal);
  Serial.println(slope);
  double retVal = previousWt+(slope*(val-previousVal));
  return retVal;
}

//============================================================

int setTableVal(char* message, int length){
  node.handler.setMessage(message, length);
  node.handler.getNextMessageField();
  char* mvStr = node.handler.getNextMessageField();
  node.handler.getNextMessageField();
  char* kgsStr = node.handler.getNextMessageField();
  node.handler.getNextMessageField();
  char* numStr = node.handler.getNextMessageField();
   
  double mv = atof(mvStr);
  double kgs = atof(kgsStr);
  int num = atoi(numStr);
  
  //int numVals;
  //EEPROMIO.load(numVals, NAMESIZE + 8);
  if(num>=numVals){
   //we're adding a value instead of replacing one
   numVals = num + 1;
   EEPROMIO.save(numVals, NAMESIZE + 8);
  }
  
  //put the value in memory
  EEPROMIO.save(mv, BASE_ADX+(8*num));
  EEPROMIO.save(kgs, BASE_ADX+(8*num)+4);
  
  Serial.print("added: ");
  Serial.print(mv);
  Serial.print(",");
  Serial.print(kgs);
  Serial.print(" - ");
  Serial.print(num);
  Serial.print(" - ");
  Serial.println(numVals);
  return 1;  
}
